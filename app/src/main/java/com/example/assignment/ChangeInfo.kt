package com.example.homework_6

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.assignment.databinding.ActivityChangeInfoBinding

class ChangeInfo : AppCompatActivity() {
    private lateinit var binding: ActivityChangeInfoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangeInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init(){
        actionBarTitle()
        listener()
    }


    private fun listener(){
        binding.sendBackBtn.setOnClickListener {
            var intent = Intent()
            intent.putExtra("name", binding.nameSend.text.toString())
            intent.putExtra("surname", binding.surnameSend.text.toString())
            intent.putExtra("email", binding.emailSend.text.toString())
            intent.putExtra("age", binding.ageSend.text.toString())
            intent.putExtra("sex", binding.sexSend.text.toString())
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }


    private fun actionBarTitle(){
        val actionBar = supportActionBar
        actionBar!!.title = "Change Info Page"
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
}