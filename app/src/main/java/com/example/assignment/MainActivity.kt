package com.example.homework_6

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.example.assignment.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var activityForResultLuncher : ActivityResultLauncher<Intent>
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        actionBarTitle()
        listeners()

        activityForResultLuncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()){
                result: ActivityResult? ->
            if(result!!.resultCode == Activity.RESULT_OK){
                binding.name.text = result.data!!.extras!!.getString("name").toString()
                binding.surname.text = result.data!!.extras!!.getString("surname").toString()
                binding.email.text = result.data!!.extras!!.getString("email").toString()
                binding.birthyear.text = result.data!!.extras!!.getString("age").toString()
                binding.sex.text = result.data!!.extras!!.getString("sex").toString()

            }
        }
    }

    private fun listeners(){
        binding.changeBtn.setOnClickListener {
            var intent = Intent(this, ChangeInfo::class.java)
            activityForResultLuncher.launch(intent)
        }
    }

    private fun actionBarTitle() {
        val actionBar = supportActionBar
        actionBar!!.title = "Home Page"
    }


}